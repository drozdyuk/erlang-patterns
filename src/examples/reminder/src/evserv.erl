-module(evserv).
-compile(export_all).

-record(state, {events, %% list of #event{} records
               clients}). %% list of Pids

-record(event, {name="",
               description,
               pid,
               timeout={{1970,1,1}, {0,0,0}}}).

start() ->
    register(?MODULE, Pid=spawn(?MODULE, init, [])),
    Pid.

start_link() ->
    register(?MODULE, Pid=spawn_link(?MODULE, init, [])),
    Pid.

terminate() ->
    ?MODULE ! shutdown.

subscribe(Pid) ->
    Ref = erlang:monitor(process, whereis(?MODULE)),
    ?MODULE ! {self(), Ref, {subscribe, Pid}},
    receive
        {Ref, ok} ->
            {ok, Ref};
        {'DOWN', Ref, process, _Pid, Reason} ->
            {error, Reason}
    after 5000 ->
            {error, timeout}
    end.

add_event(Name, Description, TimeOut) ->
    Ref = make_ref(),
    ?MODULE ! {self(), Ref, {add, Name, Description, TimeOut}},
    receive
        {Ref, Msg} -> Msg
    after 5000 ->
            {error, timeout}
    end.
        
cancel(Name) ->
    Ref = make_ref(),
    ?MODULE ! {self(), Ref, {cancel, Name}},
    receive
        {Ref, ok} -> ok
    after 5000 ->
        {error, timeout}
    end.

% Accumulate all messages during given period of time
listen(Delay) ->
    receive
        M = {done, _Name, _Description} ->
            % If messages are found, they're all taken
            % and the function returns as soon as possible
            [M | listen(0)]
    after Delay * 1000 ->
            []
    end.

loop(S = #state{}) ->
    % Pid - reply to about success of commands
    % MsgRef - unique identifier of the message 
    %          (association id as it were)
    receive
        % Client - client pid to send reminders to
        {Pid, MsgRef, {subscribe, Client}} ->
            Ref = erlang:monitor(process, Client),
            % Store the monitor's ref as the key - which is what
            % we'll get when the client goes down
            NewClients = maps:put(Ref, Client, S#state.clients),
            Pid ! {MsgRef, ok},
            loop(S#state{clients=NewClients});
        
        {Pid, MsgRef, {add, Name, Description, TimeOut}} ->
            case valid_datetime(TimeOut) of
                true ->
                    EventPid = event:start_link(Name, TimeOut),
                    NewEvents = maps:put(Name,
                                        #event{name=Name,
                                              description=Description,
                                              pid=EventPid,
                                              timeout=TimeOut},
                                         S#state.events),
                    Pid ! {MsgRef, ok},
                    loop(S#state{events=NewEvents});
                
                false ->
                    Pid ! {MsgRef, {error, bad_timeout}},
                    loop(S)
            end;
                
        {Pid, MsgRef, {cancel, Name}} ->
            Events = case maps:find(Name, S#state.events) of
                         {ok, E} -> 
                             event:cancel(E#event.pid),
                             maps:remove(Name, S#state.events);
                         error -> 
                             S#state.events
                     end,
            Pid ! {MsgRef, ok},
            loop(S#state{events=Events});
                    
        {done, Name} ->
            case maps:find(Name, S#state.events) of
                {ok, E} ->
                    send_to_clients({done, E#event.name,
                                    E#event.description},
                                   S#state.clients),
                    NewEvents = maps:remove(Name, S#state.events),
                    loop(S#state{events=NewEvents});
                error ->
                    % This may happen if we cancel and event
                    % but it fires just prior or at the same time
                    loop(S)
            end;
                
        shutdown ->
            exit(shutdown);
                
        {'DOWN', Ref, process, _Pid, _Reason} ->
            loop(S#state{clients=maps:remove(Ref, S#state.clients)});
                                                  
        code_change ->
            ?MODULE:loop(S);
                
        Unknown ->
            io:format("Unknown message: ~p~n",[Unknown]),
            loop(S)
    end.

init() ->
    loop(#state{events=maps:new(), 
                clients=maps:new()}).

valid_datetime({Date, Time}) ->
    try
        calendar:valid_date(Date) andalso valid_time(Time)
    catch
        error:function_clause -> %% not in {{Y,M,D},{H,Min,S}} format
            false
    end;
valid_datetime(_) ->
    false.

valid_time({H,M,S}) -> valid_time(H,M,S).
valid_time(H,M,S) when H >=0, H < 24,
                      M >=0, M < 60,
                      S >= 0, S < 60 -> true;
valid_time(_,_,_) -> false.

send_to_clients(Msg, ClientMap) ->
    maps:map(fun(_Ref, Pid) -> Pid ! Msg end, ClientMap).