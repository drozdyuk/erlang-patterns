% Pattern matching examples from:
% http://learnyousomeerlang.com/syntax-in-functions
-module(match).
-compile(export_all). %% replace with -export() later, for God's sake!

greet(male, Name) ->
    io:format("Hello, Mr. ~s!", [Name]);
greet(female, Name) ->
    io:format("Hello, Ms. ~s!", [Name]);
greet(_, Name) ->
    io:format("Hello, Dear ~s!", [Name]).

head([H|_]) -> H.

second([_,X|_]) -> X.

same(X,X) -> true;
same(_,_) -> false.

valid_time(Date={Y,M,D}, Time={H,Min,S}) ->
    io:format("The Date tuple (~p) says today is: ~p/~p/~p,~n",[Date,Y,M,D]),
    io:format("The time tuple (~p) indicates: ~p:~p:~p.~n", [Time,H,Min,S]);
valid_time(_,_) ->
    io:format("Stop feeding me wrong data!~n").
