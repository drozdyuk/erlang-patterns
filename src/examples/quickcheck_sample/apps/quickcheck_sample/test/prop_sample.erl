-module(prop_sample).
-include_lib("proper/include/proper.hrl").


list_tuple_gen() -> {list(int()),list(int())}.

prop_append() ->
	?FORALL({Xs, Ys}, list_tuple_gen(),
	lists:reverse(Xs++Ys)
	==
	lists:reverse(Ys) ++ lists:reverse(Xs)).

non_dup_list_gen() ->
	?LET(Lst,
		list(int()),
		elim_dups(Lst)).

elim_dups([]) -> [];
elim_dups([H|T]) -> [H|[X || X <- elim_dups(T), X =/= H]].

prop_delete_from_non_dup_list() ->
	?FORALL(L, non_dup_list_gen(),
		?IMPLIES(L /= [],
			?FORALL(I, elements(L),
				not lists:member(I,lists:delete(I,L))))).


	

% % @doc Generate a list of approximately size N (to be filtered later), 
% %      containing elements of type T, with no duplicates.
% list_at_most_n_no_dups(N, T) ->
% 	?LET(L,
% 		vector(N, T),
% 		elim_dups(L)).

% % Test the generator
% prop_list_at_most_n() ->
% 	?FORALL(L, 
% 		list_at_most_n_no_dups(10, int()),
% 		length(L) =< 10).

% % @doc Generate a list of at most size N,
% % 	   containing ONLY elements of type T.
% list_at_least_n_no_dups(N, T) ->
% 	?SUCHTHAT(
% 		X,
% 		list_at_most_n_no_dups(10 * N, T),
% 		length(X) >= N).

% % Test generator
% prop_list_at_least_n_no_dups() ->
% 	?FORALL(L,
% 		list_at_least_n_no_dups(11, int()),
% 		length(L) >= 11).

% rule_gen(L, SizeR, TypeR) ->
% 	?LET(
% 		R,
% 		list_n_no_dups(SizeR, TypeR),
% 		{rule, L, R}).

% prop_rule_gen() ->
% 	?FORALL({rule, L, R},
% 		rule_gen([a], 4, atom()),
% 		length(R) == 4).

% % @doc Generate a list of EXACTLY N elements,
% %      containing ONLY elements of type T.
% list_n_no_dups(N, T) ->
% 	?LET(
% 		X,
% 		list_at_least_n_no_dups(N,T),
% 		lists:sublist(X, N)).

% % Test gen
% prop_list_n_no_dups() ->
% 	?FORALL(L,
% 		list_n_no_dups(4, int()),
% 		sets:size(sets:from_list(L)) == length(L)).

