%% Helper module to generate random things in the domain.
-module(my_gen).
-include_lib("proper/include/proper.hrl").
-export([rule/3, rule/2, rules/0,
		rule_type/0,
		sample/1, sample_rule/0,
		pick/1, pick_rule/0]).

% @doc Generate random rules with the same A and Type.
rules() ->
	?LET(A,
		list(any()),
		?LET(Type,
			rule_type(),
			rules(Type, A)
			)
		).
% @doc Generate a random rule type
rule_type() ->
	union([left, right]).

% @doc Generate a bunch of rules, with a given
% A and Type.
rules(Type, A) ->
	list(rule(Type, A)).


% @doc Generate a rule with a given itemset,
% and a random type of B.
-spec rule(Type:: left | right, A:: term()) 
	-> {rule, list(term()), list(term())}.
rule(Type, A) ->
	rule(Type, A, any()).

rule(left, A, TypeB) ->
	?LET(I,
		list(pos_integer()),
		?LET(B,
			list(TypeB),
			{rule, A, B, I, I}));

rule(right, B, TypeA) ->
	?LET(I,
		list(pos_integer()),
		?LET(A,
			list(TypeA),
			{rule, A, B, I, I})).	

%% Pick an instance of the generator.
%% Shortcut function for SHELL experimentation only.
sample(G) -> proper_gen:sample(G).
pick(G) -> proper_gen:pick(G).
pick_rule() -> pick(rule(left, [a])).
sample_rule() -> sample(rule(left, [a])).