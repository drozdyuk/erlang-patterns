-module(kitty_server).

-export([start_link/0, order_cat/4, return_cat/2, close_shop/1, sell_cat/3]).

-record(cat, {name, color=green, price}).

%%% Client API
start_link() -> spawn_link(fun init/0).

%% Synchronous call
order_cat(Pid, Name, Color, Price) ->
	Ref = erlang:monitor(process, Pid),
	Pid ! {self(), Ref, {order, Name, Color, Price}},
	receive
		{Ref, Cat} ->
			erlang:demonitor(Ref, [flush]),
			Cat;
		{'DOWN', Ref, process, Pid, Reason} ->
			erlang:error(Reason)
	after 5000 ->
		erlang:error(timeout)
	end.

sell_cat(Pid, Cat, Price) ->
	Ref = erlang:monitor(process, Pid),
	Pid ! {self(), Ref, {sell, Cat, Price}},
	receive
		{Ref, Money} -> 
			erlang:demonitor(Ref, [flush]),
			Money;
		{'DOWN', Ref, process, Pid, Reason} ->
			erlang:error(Reason)
	after 5000 ->
		erlang:error(timeout)
	end.

%% This call is asynchronous
return_cat(Pid, Cat = #cat{}) ->
	Pid ! {return, Cat},
	ok.

%% Synchronous call
close_shop(Pid) ->
	Ref = erlang:monitor(process, Pid),
	Pid ! {self(), Ref, terminate},
	receive
		{Ref, ok} ->
			erlang:demonitor(Ref, [flush]),
			ok;
		{'DOWN', Ref, process, Pid, Reason} ->
			erlang:error(Reason)
	after 5000 ->
		erlang:error(timeout)
	end.

%% Server functions
init() -> loop([], 0).

loop(Cats, Money) ->
	receive
		{Pid, Ref, {order, Name, Color, Price}} ->
			if Cats =:= [] ->
				Pid ! {Ref, make_cat(Name, Color, Price)},
				loop(Cats, Money + Price);
			   Cats =/= [] -> % empty the stock
			    Pid ! {Ref, hd(Cats)},
			    loop(tl(Cats), Money + Price)
			end;
		{Pid, Ref, {sell, Cat, Price}} ->
			if Money >= Price ->
				Pid ! {Ref, Price}, % pay the customer!
				loop([Cat|Cats], Money - Price);
			   Money < Price ->
			    Pid ! {Ref, 0}, % pay nothing we are poor!
			    loop(Cats, Money)
			end;
		{return, Cat = #cat{}} ->
			loop([Cat|Cats], Money);
		{Pid, Ref, terminate} ->
			Pid ! {Ref, ok},
			terminate(Cats, Money);
		Unknown ->
			%% do some logging here too
			io:format("Unknown message: ~p~n", [Unknown]),
			loop(Cats, Money)
	end.

make_cat(Name, Col, Price) ->
	#cat{name=Name, color=Col, price=Price}.

terminate(Cats, Money) ->
	io:format("Made $~w~n", [Money]),
	[io:format("~p was set free.~n",[C#cat.name]) || C <- Cats],
	ok.