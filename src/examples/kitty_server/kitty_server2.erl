-module(kitty_server2).

-export([start_link/0, order_cat/4, return_cat/2, close_shop/1, sell_cat/3]).
-export([init/1, handle_call/3, handle_cast/2]).

-record(cat, {name, color=green, price}).

%%% Client API
start_link() -> my_server:start_link(?MODULE, {[],0}).

%% Synchronous call
order_cat(Pid, Name, Color, Price) ->
	my_server:call(Pid, {order, Name, Color, Price}).

sell_cat(Pid, Cat, Price) ->
	my_server:call(Pid, {sell, Cat, Price}).

%% This call is asynchronous
return_cat(Pid, Cat = #cat{}) -> my_server:cast(Pid, {return, Cat}).
	
%% Synchronous call
close_shop(Pid) -> my_server:call(Pid, terminate).

%%% Server functions
init({[],0}) -> {[], 0}.

handle_call({order, Name, Color, Price}, From, {Cats, Money}) ->
	if Cats =:= [] ->
		my_server:reply(From, make_cat(Name, Color, Price)),
		{Cats, Money + Price};
	   Cats =/= [] -> % empty the stock
	    my_server:reply(From, hd(Cats)),
	    {tl(Cats), Money + Price}
	end;

handle_call({sell, Cat, Price}, From, {Cats, Money}) ->
	if Money >= Price ->
		my_server:reply(From, Price), % pay the customer!
		{[Cat|Cats], Money - Price};
	   Money < Price ->
	    my_server:reply(From, 0), % pay nothing we are poor!
	    {Cats, Money}
	end;

handle_call(terminate, From, {Cats, Money}) ->
	my_server:reply(From, ok),
	terminate(Cats, Money).

handle_cast({return, Cat = #cat{}}, {Cats, Money}) ->
	{[Cat|Cats], Money}.

%%% Private functions
make_cat(Name, Col, Price) ->
	#cat{name=Name, color=Col, price=Price}.

terminate(Cats, Money) ->
	io:format("Made $~w~n", [Money]),
	[io:format("~p was set free.~n",[C#cat.name]) || C <- Cats],
	exit(normal).