-module(kitty_server3).
-behaviour(gen_server).
-export([order_cat/4, return_cat/2, close_shop/1, sell_cat/3, start/0, start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, code_change/3, 
	handle_info/2, terminate/2]).

-record(cat, {name, color=green, price}).

start() -> gen_server:start(?MODULE, {[], 0}, []).
start_link() -> gen_server:start_link(?MODULE, {[], 0}, []).

%%% Client api
%% Asynchronous
return_cat(Pid, Cat = #cat{}) -> gen_server:cast(Pid, {return, Cat}).

close_shop(Pid) -> gen_server:cast(Pid, stop).

%% Synchronous call
order_cat(Pid, Name, Color, Price) ->
	gen_server:call(Pid, {order, Name, Color, Price}).

sell_cat(Pid, Cat, Price) ->
	gen_server:call(Pid, {sell, Cat, Price}).


%%% Callback functions
init(State) -> {ok, State}.

%%% Server functions
%% Synchronous callbacks
handle_call({order, Name, Color, Price}, _From, {Cats, Money}) ->
	if Cats =:= [] ->
		{reply, make_cat(Name, Color, Price), {Cats, Money + Price}};
	   Cats =/= [] -> % empty the stock
	    {reply, hd(Cats), {tl(Cats), Money + Price}}
	end;

handle_call({sell, Cat, Price}, _From, {Cats, Money}) ->
	if Money >= Price ->
		{reply, Price, {[Cat|Cats], Money - Price}}; % pay the customer!
	   Money < Price ->
	    {reply, 0, {Cats, Money}} % pay nothing we are poor!
	end.

%% Asynchronous callbacks
handle_cast({return, Cat = #cat{}}, {Cats, Money}) ->
	{noreply, {[Cat|Cats], Money}};

handle_cast(stop, State) ->
	{stop, normal, State}.

%% Terminate callback
terminate(normal, {Cats, Money}) ->
	release_the_cats(Cats, Money),
	ok.

%% Code change callback
code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%% Handle unexpected messages callback
handle_info(Info, State) ->
	io:format("Unexpected message: ~w", [Info]),
	{noreply, State}.

%%% Private functions
make_cat(Name, Col, Price) ->
	#cat{name=Name, color=Col, price=Price}.

release_the_cats(Cats, Money) ->
	io:format("Made $~w~n", [Money]),
	[io:format("~p was set free.~n",[C#cat.name]) || C <- Cats].