%%%-------------------------------------------------------------------
%%% @author myrakumka
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%% From Programming Erlang 2nd
%%% @end
%%% Created : 06. Jan 2016 8:20 PM
%%%-------------------------------------------------------------------
-module(clock).
-author("myrakumka").

%% API
-export([start/2, stop/0]).

stop() ->
  clockService ! cancel.
  %unregister(clockService).

clockS(Time, Fun) ->
  receive
    cancel ->
      void
    after Time->
      Fun(),
      clockS(Time,Fun)
  end.

start(Time, Fun) ->
  register(clockService, spawn(fun()->clockS(Time, Fun) end)).
