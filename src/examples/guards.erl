% Pattern matching with guards, from:
% http://learnyousomeerlang.com/syntax-in-functions
-module(guards).
-compile(export_all). %% replace with -export() later, for God's sake!

old_enough(X) when X >= 16 -> true;
old_enough(_) -> false.

% How to represent over 16 and under 104? Use "and":
right_age(X) when X >= 16, X =< 104 ->
    true;
right_age(_) -> false.

% ... alternatively use the "or" connective:
wrong_age(X) when X < 16; X > 104 ->
    true;
wrong_age(_) -> false.