-module(linkmon).
-compile(export_all).

myproc() ->
    timer:sleep(5000),
    exit(reason).

chain(0) ->
    receive
        _ -> ok
    after 2000 ->
        exit("Chain dies here")
    end;

chain(N) ->
    %%    Pid = spawn(fun() -> chain(N-1) end),
    %%    link(Pid),
    % This is safer way of doing above:
    spawn_link(fun() -> chain(N-1) end),
    receive
        _ -> ok
    end.
%% How this looks from the shell's point of view:
%% linkmon:chain(3).
%%[shell] == [3] == [2] == [1] == [0]
%%[shell] == [3] == [2] == [1] == *dead*
%%[shell] == [3] == [2] == *dead*
%%[shell] == [3] == *dead*
%%[shell] == *dead*
%%*dead, error message shown*
%%[shell] <-- restarted
%%
%%Its important to note that link(spawn(Function)) or link(spawn(M,F,A)) happens in more than one step. In some cases, it is possible for a process to die before the link has been set up and then provoke unexpected behavior. 
%%For this reason, the function spawn_link/1-3 has been added to the language. It takes the same arguments as spawn/1-3, creates a process and links it as if link/1 had been there, except it's all done as an atomic operation (the operations are combined as a single one, which can either fail or succeed, but nothing else). This is generally considered safer and you save a set of parentheses too.

%% Setting a trac exit to true on a process makes it 
%% a system process, and allows it to monitor death instead
%% of dying with it:
%%1> process_flag(trap_exit, true).
%%true
%%2> spawn_link(fun() -> linkmon:chain(3) end).
%%<0.49.0>
%%3> receive X -> X end.
%%{'EXIT',<0.49.0>,"chain dies here"}
%%Ah! Now things get interesting. To get back to our drawings, what happens is now more like this:
%%
%%[shell] == [3] == [2] == [1] == [0]
%%[shell] == [3] == [2] == [1] == *dead*
%%[shell] == [3] == [2] == *dead*
%%[shell] == [3] == *dead*
%%[shell] <-- {'EXIT,Pid,"chain dies here"} -- *dead*
%%[shell] <-- still alive!


% Different ways a system process or a regular process
% might see another terminating:

%%Exception source: spawn_link(fun() -> ok end)
%%Untrapped Result: - nothing -
%%Trapped Result: {'EXIT', <0.61.0>, normal}
%%The process exited normally, without a problem. Note that this looks a bit like the result of catch exit(normal), except a PID is added to the tuple to know what processed failed.

%%Exception source: spawn_link(fun() -> exit(reason) end)
%%Untrapped Result: ** exception exit: reason
%%Trapped Result: {'EXIT', <0.55.0>, reason}
%%The process has terminated for a custom reason. In this case, if there is no trapped exit, the process crashes. Otherwise, you get the above message.

%%Exception source: spawn_link(fun() -> exit(normal) end)
%%Untrapped Result: - nothing -
%%Trapped Result: {'EXIT', <0.58.0>, normal}
%%This successfully emulates a process terminating normally. In some cases, you might want to kill a process as part of the normal flow of a program, without anything exceptional going on. This is the way to do it.

%%Exception source: spawn_link(fun() -> 1/0 end)
%%Untrapped Result: Error in process <0.44.0> with exit value: {badarith, [{erlang, '/', [1,0]}]}
%%Trapped Result: {'EXIT', <0.52.0>, {badarith, [{erlang, '/', [1,0]}]}}
%%The error ({badarith, Reason}) is never caught by a try ... catch block and bubbles up into an 'EXIT'. At this point, it behaves exactly the same as exit(reason) did, but with a stack trace giving more details about what happened.

%%Exception source: spawn_link(fun() -> erlang:error(reason) end)
%%Untrapped Result: Error in process <0.47.0> with exit value: {reason, [{erlang, apply, 2}]}
%%Trapped Result: {'EXIT', <0.74.0>, {reason, [{erlang, apply, 2}]}}
%%Pretty much the same as with 1/0. That's normal, erlang:error/1 is meant to allow you to do just that.

%%Exception source: spawn_link(fun() -> throw(rocks) end)
%%Untrapped Result: Error in process <0.51.0> with exit value: {{nocatch, rocks}, [{erlang, apply, 2}]}
%%Trapped Result: {'EXIT', <0.79.0>, {{nocatch, rocks}, [{erlang, apply, 2}]}}
%%Because the throw is never caught by a try ... catch, it bubbles up into an error, which in turn bubbles up into an EXIT. Without trapping exit, the process fails. Otherwise it deals with it fine.


%%Then there's exit/2. This one is the Erlang process equivalent of a gun. It allows a process to kill another one from a distance, safely. Here are some of the possible calls:
%%
%%Exception source: exit(self(), normal)
%%Untrapped Result: ** exception exit: normal
%%Trapped Result: {'EXIT', <0.31.0>, normal}
%%When not trapping exits, exit(self(), normal) acts the same as exit(normal). Otherwise, you receive a message with the same format you would have had by listening to links from foreign processes dying.
%%
%%Exception source: exit(spawn_link(fun() -> timer:sleep(50000) end), normal)
%%Untrapped Result: - nothing -
%%Trapped Result: - nothing -
%%This basically is a call to exit(Pid, normal). This command doesn't do anything useful, because a process can not be remotely killed with the reason normal as an argument.
%%
%%Exception source: exit(spawn_link(fun() -> timer:sleep(50000) end), reason)
%%Untrapped Result: ** exception exit: reason
%%Trapped Result: {'EXIT', <0.52.0>, reason}
%%This is the foreign process terminating for reason itself. Looks the same as if the foreign process called exit(reason) on itself.
%%
%%Exception source: exit(spawn_link(fun() -> timer:sleep(50000) end), kill)
%%Untrapped Result: ** exception exit: killed
%%Trapped Result: {'EXIT', <0.58.0>, killed}
%%Surprisingly, the message gets changed from the dying process to the spawner. The spawner now receives killed instead of kill. That's because kill is a special exit signal. More details on this later.
%%Exception source: exit(self(), kill)
%%Untrapped Result: ** exception exit: killed
%%Trapped Result: ** exception exit: killed
%%Oops, look at that. It seems like this one is actually impossible to trap. Let's check something.
%%
%%Exception source: spawn_link(fun() -> exit(kill) end)
%%Untrapped Result: ** exception exit: killed
%%Trapped Result: {'EXIT', <0.67.0>, kill}
%%Now that's getting confusing. When another process kills itself with exit(kill) and we don't trap exits, our own process dies with the reason killed. However, when we trap exits, things don't happen that way.

start_critic() ->
    spawn(?MODULE, critic, []).

judge(Pid, Band, Album) ->
    Pid ! {self(), {Band, Album}},
    receive
        {Pid, Criticism} -> Criticism
    after 2000 ->
        timeout
    end.

critic() ->
    receive
        {From, {"Rage Against the Turing Machine", "Unit Testify"}} ->
            From ! {self(), "They are great!"};
        {From, {"System of a Downtime", "Memoize"}} ->
            From ! {self(), "They're not Johnny Crash but they're good."};
        {From, {"Johnny Crash", "The Token Ring of Fire"}} ->
            From ! {self(), "Simply incredible."};
        {From, {_Band, _Album}} ->
            From ! {self(), "They are terrible!"}
    end,
critic().

% basic supervisor process
start_critic2() ->
    spawn(?MODULE, restarter, []).

restarter() ->
    process_flag(trap_exit, true),
    Pid = spawn_link(?MODULE, critic2, []),
    register(critic, Pid),
    receive
        {'EXIT', Pid, normal} -> % not a crash
            ok;
        {'EXIT', Pid, shutdown} -> % manual termination, not a crash
            ok;
        {'EXIT', Pid, _} ->
            restarter()
    end.
%What we need to do is then remove the need to pass in a Pid from the abstraction functions. Let's try this one:
judge2(Band, Album) ->
    Ref = make_ref(),
    critic ! {self(), Ref, {Band, Album}},
    receive
        {Ref, Criticism} -> Criticism
    after 2000 ->
        timeout
    end.

critic2() ->
    receive
        {From, Ref, {"Rage Against the Turing Machine", "Unit Testify"}} ->
            From ! {Ref, "They are great!"};
        {From, Ref, {"System of a Downtime", "Memoize"}} ->
            From ! {Ref, "They're not Johnny Crash but they're good."};
        {From, Ref, {"Johnny Crash", "The Token Ring of Fire"}} ->
            From ! {Ref, "Simply incredible."};
        {From, Ref, {_Band, _Album}} ->
            From ! {Ref, "They are terrible!"}
    end,
    critic2().
