-module(test).
-record(state, {name}).
-compile(export_all).

make(Name) -> #state{name=Name}.

check(S=#state{}) ->
	io:format("Name is ~p~n", [S]).
% This will never be reached, because above already matches everything!
% check(#state{name=Name}) ->
% 	io:format("Name is not empty it's ~w~n", [Name]);=.
