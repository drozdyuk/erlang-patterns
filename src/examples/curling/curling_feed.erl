-module(curling_feed).
-behaviour(gen_event).
-export([init/1, handle_event/2, handle_call/2, handle_info/2, code_change/3, 
	terminate/2]).

%% Events:
% {set_teams, TeamA, TeamB} translated to curling_scoreboard_hw:set_teams(TeamA,TeamB)
% {add_points, Team, N} - translated to N-calls to ...hw:add_point(Team)
% next_round - which gets translated to a single call with the same name

% Since tuple {Args, Term} is used for other purposes
init([Pid]) -> {ok, Pid}.

handle_event(Event, Pid) -> 
	Pid ! {curling_feed, Event},
	{ok, Pid}.

handle_call(_Request, Pid) -> {ok, ok, Pid}.

handle_info(_Info, Pid) -> {ok, Pid}.

code_change(_OldVsn, Pid, _Extra) -> {ok, Pid}.

terminate(_Arg, _Pid) -> ok.