-module(curling_scoreboard).
-behaviour(gen_event).
-export([init/1, handle_event/2, handle_call/2, handle_info/2, code_change/3, 
	terminate/2]).

%% Events:
% {set_teams, TeamA, TeamB} translated to curling_scoreboard_hw:set_teams(TeamA,TeamB)
% {add_points, Team, N} - translated to N-calls to ...hw:add_point(Team)
% next_round - which gets translated to a single call with the same name

init([]) -> {ok, []}.

handle_event({set_teams, TeamA, TeamB}, State) -> 
	curling_scoreboard_hw:set_teams(TeamA, TeamB),
	{ok, State};
handle_event({add_points, Team, N}, State) ->
	[curling_scoreboard_hw:add_point(Team) || _ <- lists:seq(1,N)],
	{ok, State};
handle_event(next_round, State) -> 
	curling_scoreboard_hw:next_round(),
	{ok, State};
handle_event(_Event, State) -> {ok, State}.

handle_call(_Request, State) -> {ok, ok, State}.

handle_info(_Info, State) -> {ok, State}.

code_change(_OldVsn, State, _Extra) -> {ok, State}.

terminate(_Arg, _State) -> ok.
