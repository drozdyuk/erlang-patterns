-module(curling_accumulator).
-behaviour(gen_event).
-export([init/1, handle_event/2, handle_call/2, handle_info/2, code_change/3, 
	terminate/2]).

-record(game, {teams, round=0}).

%% Events:
% {set_teams, TeamA, TeamB} translated to curling_scoreboard_hw:set_teams(TeamA,TeamB)
% {add_points, Team, N} - translated to N-calls to ...hw:add_point(Team)
% next_round - which gets translated to a single call with the same name

% Since tuple {Args, Term} is used for other purposes
init([]) -> {ok, #game{}}.

handle_event({set_teams, TeamA, TeamB}, Game) -> 
	{ok, Game#game{teams = #{TeamA=>0, TeamB=>0} }};
handle_event({add_points, Team, N}, Game=#game{ teams=T }) ->
	Score = maps:get(Team, T, 0),
	Teams = maps:put(Team, Score + N, T),
	{ok, Game#game{teams=Teams}};
% handle_event({add_points, Team, N}, Game=#game{teamb=Team, scoreb=Score}) ->
% 	{ok, Game#game{scoreb=Score+N}};
handle_event(next_round, Game=#game{ teams=T, round=Round }) -> 
	% reset all scores to zero and increase round counter
	Zero = fun(_K, _V) -> 0 end,
	Teams = maps:map(Zero, T),
	{ok, Game#game{ teams=Teams, round=Round+1 }};
handle_event(_Event, State) -> {ok, State}.

handle_call(game_data, State=#game{teams=Teams,round=Round}) -> 
	Ret = {maps:to_list(Teams), {round, Round}},
	{ok, Ret, State}.

handle_info(_Info, State) -> {ok, State}.

code_change(_OldVsn, State, _Extra) -> {ok, State}.

terminate(_Arg, _State) -> ok.
