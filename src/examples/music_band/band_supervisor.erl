-module(band_supervisor).
-behaviour(supervisor).

-export([start_link/1]).
-export([init/1]).

%% Three types of supervisors:
% lenient - one_for_one, will restart 3 times in 60s before giving up
% angry - rest_for_one, will restart 2 times in 60s before giving up
% jerk - one_for_all, will restart 1 time in 60s before giving up
start_link(Type) -> supervisor:start_link({local, ?MODULE}, ?MODULE, Type).


init(lenient) 	-> init({one_for_one, 3, 60});
init(angry) 	-> init({rest_for_one, 2, 60});
init(jerk) 		-> init({one_for_all, 1, 60});
init(jamband)   -> 
	SupFlags = #{strategy => simple_one_for_one, intensity => 3, period => 60},
	JamMusician = #{ id => jam_musician,
           		     start => {musicians, start_link, []},
           		     restart => temporary, 
           		     shutdown => 1000, 
           		     type => worker, 
           		     modules => [musicians]},
	ChildrenSpecs = [JamMusician],
	{ok, {SupFlags, ChildrenSpecs}};

init({Strategy, Intensity, Period}) ->
	SupFlags = #{strategy => Strategy, intensity => Intensity, period => Period},
	Singer = #{	id => singer, 
				start => {musicians, start_link, [singer, good]},
				restart => permanent,
				shutdown => 1000,
				type => worker,
				modules => [musicians]},
	Bass = #{	id => bass,
				start => {musicians, start_link, [bass, good]},
				restart => temporary,
				shutdown => 1000,
				type => worker,
				modules => [musicians]},
	Drum = #{	id => drum,
				start => {musicians, start_link, [drum, bad]},
				restart => transient,
				shutdown => 1000,
				type => worker,
				modules => [musicians]},
	Keytar = #{ id => keytar,
				start => {musicians, start_link, [keytar, good]},
				restart => transient,
				shutdown => 1000,
				type => worker,
				modules => [musicians]},
	ChildSpecs = [Singer, Bass, Drum, Keytar],
	{ok, {SupFlags, ChildSpecs}}.