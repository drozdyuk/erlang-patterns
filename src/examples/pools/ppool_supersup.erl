-module(ppool_supersup).
-behaviour(supervisor).
-export([start_link/0, stop/0, start_pool/3, stop_pool/1]).
-export([init/1]).

start_link() -> supervisor:start_link({local, ppool}, ?MODULE, []).

%% technically, a supervisor cannot be killed in an easy way,
%% let's do it brutally (The reason for this is that the OTP framework provides 
%% a well-defined shutdown procedure for all supervisors -- next chapter!)
stop() ->
	case whereis(ppool) of
		P when is_pid(P) ->
			exit(P, kill);
		_ -> ok
	end.

init([]) ->
	MaxRestart = 6,
	MaxTime = 3600,
	{ok, {{one_for_one, MaxRestart, MaxTime}, []}}.

start_pool(Name, Limit, MFA) ->
	ChildSpec = #{ 	id => Name,
					start => {ppool_sup, start_link, [Name, Limit, MFA]},
					restart => permanent,
					shutdown => 10500,
					type => supervisor,
					modules => [ppool_sup]},
	supervisor:start_child(ppool, ChildSpec).

stop_pool(Name) ->
	supervisor:terminate_child(ppool, Name),
	supervisor:delete_child(ppool, Name).