% If statement syntax from:
% http://learnyousomeerlang.com/syntax-in-functions
-module(ifstm).
-compile(export_all). %% replace with -export() later, for God's sake!

%% note, this one would be better as a pattern match in function heads!
%% I'm doing it this way for the sake of the example.
help_me(Animal) ->
    Talk = if Animal == cat  -> "meow";
            Animal == beef -> "mooo";
            Animal == dog  -> "bark";
            Animal == tree -> "bark";
            true -> "fgdadfgna"
           end,
    {Animal, "says " ++ Talk ++ "!"}.
%'Else' or 'true' branches should be "avoided" altogether: ifs are usually easier to read when you cover all logical ends rather than relying on a "catch all" clause.

% E.g. replace stuff on left with stuff on right:
%%if X > Y -> a()		if X > Y  -> a()
%%	 ; true  -> b()		 ; X =< Y -> b()
%%	end		     	end
%%
%%	if X > Y -> a()		if X > Y -> a()
%%	 ; X < Y -> b()		 ; X < Y -> b()
%%	 ; true  -> c()		 ; X ==Y -> c()
%%	end			end