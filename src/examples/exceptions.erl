-module(exceptions).
-compile(export_all).

errors(F) ->
    try F() of
        _ -> ok
    catch
        error:Error -> {error, caught, Error}
    end.

exits(F) ->
    try F() of
        _ -> ok
    catch
        exit:Exit -> {exit, caught, Exit}
    end.

throws(F) ->
    try F() of
        _ -> ok
    catch
        Throw -> {throw, caught, Throw}
    end.

% Function that breaks in different ways!
sword(1) -> throw(slice);
sword(2) -> erlang:error(cut_arm);
sword(3) -> exit(cut_leg);
sword(4) -> throw(punch);
sword(5) -> exit(cross_bridge).

black_knight(Attack) when is_function(Attack, 0) ->
    try Attack() of
        _ -> "None shall pass."
    catch
        throw:slice -> "It is but a scratch.";
        error:cut_arm -> "I've had worse.";
        exit:cut_leg -> "Come on you pansy!";
        _:_ -> "Just a flesh wound."
    end.

talk() -> "Blah blah!".

% catch can be used alone and acts as follows (not recommended! //Andriy):
%% 1> catch throw(whoa).
%% whoa
%% 2> catch exit(die).
%% {'EXIT',die}
%% 3> catch 1/0.
%% {'EXIT',{badarith,[{erlang,'/',[1,0]},
%%   {erl_eval,do_apply,5},
%%   {erl_eval,expr,5},
%%   {shell,exprs,6},
%%   {shell,eval_exprs,6},
%%   {shell,eval_loop,3}]}}
%% 4> catch 2+2.
%% 4
catcher(X, Y) ->
    case catch X/Y of
        {'EXIT', {badarith, _}} -> "uh oh";
        N -> N
    end.   
% WARNING: Because we're behind a catch, we can never know if the function threw an exception or if it returned an actual value!
% This might not really happen a whole lot in practice, but it's still a wart big enough to have warranted the addition of the try ... catch construct in the R10B release.