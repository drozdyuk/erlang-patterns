-module(multiproc).
-compile(export_all).
% How a sleep function might be implemented
sleep(T) ->
    receive
    after T ->
          ok
    end.

% In the case above, anything matches. As long as there are messages, the flush/0 function will recursively call itself until the mailbox is empty.
flush() ->
    receive 
        _ -> flush()
    after 0 ->
        ok
end.

% This after 0 enables - Selective receive 
%%This function will build a list of all messages with those with a priority above 10 coming first:
%%
%%1> c(multiproc).
%%{ok,multiproc}
%%2> self() ! {15, high}, self() ! {7, low}, self() ! {1, low}, self() ! {17, high}.      
%%{17,high}
%%3> multiproc:important().
%%[high,high,low,low]
important() ->
    receive
        {Priority, Message} when Priority > 10 ->
            [Message | important()]
        after 0 ->
            normal()
    end.

normal() ->
    receive
        {_, Message} ->
            [Message | normal()]
    after 0 ->
        []
    end.