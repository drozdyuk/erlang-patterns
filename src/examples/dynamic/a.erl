% This will dynamically load the latest version of b:x()
% if it is recompiled during the running of this module.
% I.e. if started like:
%   c(b).
%   start(hello1).
% Just change b:x() to return something else then:
%   c(b).
%   start(hello2).
% And you will see that you now have two processes running with diff versions of x().
% Further recompiling and repeating above evicts the oldest version.
-module(a).
-compile(export_all).

start(Tag) ->
    spawn(fun() -> loop(Tag) end).
    
loop(Tag) ->
    sleep(),
    Val = b:x(),
    io:format("Vsn3 (~p) b:x() = ~p~n", [Tag, Val]),
    loop(Tag).

sleep() ->
    receive 
        after 3000 -> true
   end.