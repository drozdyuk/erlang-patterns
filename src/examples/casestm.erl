% case statement example, from:
% http://learnyousomeerlang.com/syntax-in-functions
-module(casestm).
-compile(export_all). %% replace with -export() later, for God's sake!

% append function for sets (worst implementation possible in terms of efficiency)
insert(X,[]) ->
    [X];
insert(X,Set) ->
    case lists:member(X,Set) of
        true  -> Set;
        false -> [X|Set]
    end.