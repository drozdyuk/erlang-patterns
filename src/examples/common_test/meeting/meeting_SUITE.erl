% The suite, named meeting_SUITE.erl, will be based on the simple idea of trying
% to provoke a race condition that will mess up with the registration. We'll
% thus have three test cases, each representing a group. Carla will represent
% women, Mark will represent men, and a dog will represent a group of animals
% that somehow decided it wanted to hold a meeting with human-made tools:
-module(meeting_SUITE).
% WARNING: Don't forget to export the two setup and teardown functions,
% otherwise nothing will run in parallel.
-export([init_per_group/2, end_per_group/2, groups/0]).
-export([all/0, carla/1, mark/1, dog/1, all_same_owner/1]).
-include_lib("common_test/include/ct.hrl").

carla(_Config) ->
	meeting:book_room(women),
	timer:sleep(10),
	meeting:rent_projector(women),
	timer:sleep(10),
	meeting:use_chairs(women).

mark(_Config) ->
	meeting:rent_projector(men),
	timer:sleep(10),
	meeting:use_chairs(men),
	timer:sleep(10),
	meeting:book_room(men).

dog(_Config) ->
	meeting:rent_projector(animals),
	timer:sleep(10),
	meeting:use_chairs(animals),
	timer:sleep(10),
	meeting:book_room(animals).

all_same_owner(_Config) ->
	[{_, Owner}, {_, Owner}, {_, Owner}] = meeting:get_all_bookings().

all() -> [{group, session}].

groups() -> [{session,
               [],
               [{group, clients}, all_same_owner]}, % will be run in this order
             {clients, % group of tests
  		       [parallel, {repeat, 10}],
			   [carla, mark, dog]}
            ].

init_per_group(session, Config) ->
	meeting:start(),
	Config;
init_per_group(_, Config) -> 
	Config.

end_per_group(session, _Config) ->
	meeting:stop();
end_per_group(_, _Config) -> 
	ok.	
