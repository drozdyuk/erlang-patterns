-module(mafiapp_app).

-behaviour(application).

% A.D.: See mafiapp.erl for actual app.

%% Application callbacks
-export([start/2
        ,stop/1]).

start(_StartType, _StartArgs) ->
	mnesia:wait_for_tables([mafiapp_friends, mafiapp_services, mafiapp_enemies], 5000),
    mafiapp_sup:start_link().

stop(_State) ->
    ok.
