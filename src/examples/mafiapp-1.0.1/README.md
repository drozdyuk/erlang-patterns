mafiapp-1.0.1
==========

An OTP application that uses Query List Comprehensions parse transforms, to
make working with mnesia easier:
http://learnyousomeerlang.com/mnesia

Build
-----

    $ rebar3 compile
