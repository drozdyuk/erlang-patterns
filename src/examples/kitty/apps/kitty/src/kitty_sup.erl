%%%-------------------------------------------------------------------
%% @doc kitty top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(kitty_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->

    KittySpec = #{id => kitty,
      start => {kitty, start_link, []},
      restart => permanent,
      shutdown => brutal_kill,
      type => worker,
      modules => [kitty]},
	Children = [KittySpec],
	RestartStrategy = {one_for_one, 0, 1},
    {ok, { RestartStrategy, Children} }.

%%====================================================================
%% Internal functions
%%====================================================================
