-module(template).

-export([path/0, path/1]).

path() ->
	Dir = code:priv_dir(kitty_api),
	filename:join([Dir, "templates"]).

path(Name) ->
	FName = filename:flatten([Name, ".html"]),
	filename:join([path(), FName]).
	