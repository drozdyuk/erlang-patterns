-module(kitty_api_app).

-behaviour(application).

%% Application callbacks
-export([start/2
        ,stop/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
	% Compile templates into a module called - "dtl"
    erlydtl:compile_dir(template:path(), dtl, [{out_dir, "."}]),

	PathsList = [
        {"/",       handle_index,    []},
		{"/order/", handle_order, []},
        {"/success", handle_success, []},
        {"/assets/[...]", cowboy_static, {priv_dir, kitty_api, "static/assets"}}

	],

    Dispatch = cowboy_router:compile([
        {'_', PathsList}
    ]),
    cowboy:start_http(my_http_listener, 100, [{port, 8080}],
        [{env, [{dispatch, Dispatch}]}]
    ),
    kitty_api_sup:start_link().

%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%====================================================================
%% Internal functions
%%====================================================================
