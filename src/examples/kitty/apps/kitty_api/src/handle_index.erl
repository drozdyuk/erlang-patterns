-module(handle_index).
-export([init/3, content_types_provided/2, to_html/2, allowed_methods/2]).

init(_Type, _Req, _Opts) -> {upgrade, protocol, cowboy_rest}.

allowed_methods(Req, State) -> {[<<"GET">>, <<"HEAD">>], Req, State}.

content_types_provided(Req, State) ->
	{[{{<<"text">>, <<"html">>, '*'}, to_html}],
		Req, State}.

to_html(Req, State) ->
	Body = dtl:index([]),
    {Body, Req, State}.
