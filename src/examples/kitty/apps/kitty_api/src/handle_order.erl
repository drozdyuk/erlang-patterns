-module(handle_order).
-export([init/3, content_types_accepted/2, from_urlencoded/2, 
		content_types_provided/2, to_html/2, allowed_methods/2]).

init(_Type, _Req, _Opts) -> {upgrade, protocol, cowboy_rest}.

allowed_methods(Req, State) -> {[<<"GET">>, <<"HEAD">>, <<"POST">>, <<"OPTIONS">>], Req, State}.

content_types_provided(Req, State) ->
	{[{{<<"text">>, <<"html">>, '*'}, to_html}],
		Req, State}.

content_types_accepted(Req, State) ->
	{[
		{<<"application/x-www-form-urlencoded">>, from_urlencoded}
	 ],
	 Req, State}.

from_urlencoded(Req1, State) ->
	{ok, ReqBody, Req} = cowboy_req:body_qs(Req1),

	% returns tuple or false if not found
	{_, NameReq} = lists:keyfind(<<"name">>, 1, ReqBody),
	{_, ColorReq} = lists:keyfind(<<"color">>, 1, ReqBody),
	{_, PriceReqStr} = lists:keyfind(<<"price">>, 1, ReqBody),
	{PriceReq, _} = string:to_integer(binary_to_list(PriceReqStr)),
	%{cat, Name, Color, Price} = 
	kitty:order_cat(NameReq, ColorReq, PriceReq), 
	% Body = dtl:ordered([{name, Name}, {color, Color}, {price, Price}]),    
    {{true, "success"}, Req, State}.

to_html(Req, State) ->
	Body = dtl:order_post([]),
    {Body, Req, State}.
