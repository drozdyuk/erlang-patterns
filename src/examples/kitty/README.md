kitty
=====

This is an example of an app that mixes two OTP apps, 
with one of them (kitty_api) being an HTTP API app.
It uses templating engine to render the response.

Build
-----

    $ rebar3 compile
