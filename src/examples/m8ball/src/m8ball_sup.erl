%%%-------------------------------------------------------------------
%% @doc m8ball top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(m8ball_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({global, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
    {ok, {{one_for_all, 1, 10}, 
	      [{m8ball, {m8ball_server, start_link, []},
	      permanent,
	      5000,
	      worker,
	      [m8ball_server]
    }]}}.

%%====================================================================
%% Internal functions
%%====================================================================
