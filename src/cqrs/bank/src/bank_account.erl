-module(bank_account).
-behaviour(gen_server).

-export([new/0, create/2, deposit_money/2, withdraw_money/2]).
-export([process_unsaved_changes/2, load_from_history/2]).
% gen_server callbacks
-export([init/1, terminate/2, handle_call/3, handle_cast/2, handle_info/2, code_change/3]).

-record(state, {id, date_created, balance=0, changes=[]}).
-define(PROCESS_TIME_OUT, 45000).

-include("bank_data_structures.hrl").

%% API
new() ->
	{ok, Pid} = gen_server:start_link(?MODULE, [], []),
	Pid.

create(Pid, Id) ->
	gen_server:call(Pid, {create, Id}).

deposit_money(Pid, Amount) ->
	gen_server:call(Pid, {deposit_money, Amount}).

withdraw_money(Pid, Amount) ->
	gen_server:call(Pid, {withdraw_money, Amount}).

process_unsaved_changes(Pid, Saver) ->
	gen_server:cast(Pid, {process_unsaved_changes, Saver}).

load_from_history(Pid, Events) ->
 	gen_server:cast(Pid, {load_from_history, Events}).

%% Callbacks
init([]) ->
	State = #state{},
	{ok, State, ?PROCESS_TIME_OUT}.

handle_cast({load_from_history, Events}, _State) ->
	NewState = apply_many_events(Events, #state{}),
	{noreply, NewState};

handle_cast({process_unsaved_changes, Saver}, State) ->
	Id = State#state.id,
	Saver(Id, lists:reverse(State#state.changes)),
	NewState = State#state{changes=[]},
	{noreply, NewState};

handle_cast(_Request, State) -> {noreply, State}.

code_change(_OldVsn, State, _Extra) -> {ok, State}.

handle_call({create, Id}, _From, State) ->
	%% TODO: check if it's already been created
	Event = #bank_account_created{id=Id, date_created=erlang:localtime()},
	NewState = apply_new_event( Event, State),
	{reply, ok, NewState};

handle_call({deposit_money, Amount}, _From, State) ->
	NewBalance = State#state.balance + Amount,
	Id = State#state.id,
	Event = #bank_account_money_deposited{id=Id, amount=Amount,
				new_balance=NewBalance,transaction_date=erlang:localtime()},
	NewState = apply_new_event(Event, State),
	{reply, ok, NewState};

handle_call({withdraw_money, Amount}, _From, State) ->
	NewBalance = State#state.balance - Amount,
	Id = State#state.id,

	case(NewBalance < 0) of
		false ->
			Event = #bank_account_money_withdrawn{id=Id, amount=Amount,
						new_balance=NewBalance,transaction_date=erlang:localtime()},
			NewState = apply_new_event(Event, State),
			{reply, ok, NewState};
		true ->
			Event = #bank_account_payment_declined{id=Id,amount=Amount,
						transaction_date=erlang:localtime()},
			NewState = apply_new_event(Event, State),
			{reply, declined, NewState}
	end;
handle_call(Command, _From, State) ->
	error_logger:warn_msg("Unexpected command (~p)~n", [Command]),
	{noreply, State}.

handle_info(timeout, #state{id=Id}=State) ->
	bank_account_repository:remove_from_cache(Id),
	{stop, timeout, State};
handle_info(timeout, State) ->
	{stop, timeout, State};
handle_info(Unknown, State) ->
	error_logger:warning_msg("Received unknown message (~p)~n", [Unknown]),
	{noreply, State}.


terminate(_Reason, _State) -> ok.

%% Internals
apply_new_event(Event, State) ->
	NewState = apply_event(Event, State),
	CombinedChanges = [Event] ++ NewState#state.changes,
	NewState#state{changes=CombinedChanges}.

apply_event(#bank_account_created{id=Id,date_created=DateCreated}, State) ->
	bank_account_repository:add_to_cache(Id),
	State#state{id=Id, date_created=DateCreated};
apply_event(#bank_account_money_deposited{amount=Amount}, #state{balance=Balance}=State) ->
	State#state{balance = Balance + Amount};

apply_event(#bank_account_money_withdrawn{amount=Amount}, #state{balance=Balance}=State) ->
	State#state{balance = Balance - Amount};

apply_event(_Event, State)->
	State. % For some events, we don't have state to mutate

apply_many_events([], State) ->
	State;
apply_many_events([Event|Rest], State) ->
	NewState = apply_event(Event, State),
	apply_many_events(Rest, NewState).
