-module(bank).

%% The public client API for the bank application
-export([open/0,close/0,
		create/1,deposit/2, withdraw/2,
		check_balance/1]).

-include("bank_data_structures.hrl").

open() ->
	application:start(bank).

close() ->
	application:stop(bank).

create(Account) ->
	bank_command_handler:create_bank_account(Account).

deposit(Account, Amount) ->
	bank_command_handler:deposit_money_into_bank_account(Account, Amount).

withdraw(Account, Amount) ->
	bank_command_handler:withdraw_money_from_bank_account(Account, Amount).

%% @spec check_balance(Account) -> {ok, Value} | no_such_account
-spec check_balance(term()) -> {ok, term()} | no_such_account.
check_balance(Account) ->
	Details = bank_read_store:get_bank_account_details(),
	Dict = dict:from_list(Details),
	case dict:find(Account, Dict) of
		{ok, Value} -> Value;
		_ -> no_such_account
	end.
