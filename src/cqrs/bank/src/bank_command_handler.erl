-module(bank_command_handler).
-behavior(gen_server).

%% API Function Expoerts
-export([start_link/0, create_bank_account/1, deposit_money_into_bank_account/2,
	     withdraw_money_from_bank_account/2]).

% gen_server callbacks
-export([init/1, terminate/2, handle_call/3, handle_cast/2, handle_info/2, code_change/3]).

-include("bank_data_structures.hrl").

%% API
start_link() -> gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

create_bank_account(Id) -> 
  gen_server:call(?MODULE, {create_bank_account, Id}).

deposit_money_into_bank_account(Account, Amount) -> 
  gen_server:call(?MODULE, {deposit_money_into_bank_account, {Account, Amount}}).

withdraw_money_from_bank_account(Account, Amount) ->
  gen_server:call(?MODULE, {withdraw_money_from_bank_account, {Account, Amount}}).


%% Gen Server Callbacks
init([]) ->	{ok, []}.

handle_cast(_Request, State) -> {noreply, State}.

handle_call({create_bank_account, Id}, _From, State) ->
	case bank_account_repository:get_by_id(Id) of
		not_found ->
			Pid = bank_account:new(),
			bank_account:create(Pid, Id),
			bank_account_repository:save(Pid),
			{reply, created, State};
		_ -> 	
			{reply, exists, State}
	end;

handle_call({deposit_money_into_bank_account, {Id, Amount}}, _From, State) ->
	case bank_account_repository:get_by_id(Id) of
		not_found ->
			{reply, no_such_account, State};
		{ok,Pid} ->
			Result = bank_account:deposit_money(Pid, Amount),
			bank_account_repository:save(Pid),
			{reply, Result, State}
	end;

handle_call({withdraw_money_from_bank_account, {Id, Amount}}, _From, State) ->
	case bank_account_repository:get_by_id(Id) of
		not_found ->
			{reply, not_found, State};
		{ok, Pid} ->
			Result = bank_account:withdraw_money(Pid, Amount),
			bank_account_repository:save(Pid),
			{reply, Result, State}
	end;

handle_call(Command, _From, State) ->
	error_logger:warn_msg("Unexpected command (~p)~n", [Command]),
	{noreply, State}.

handle_info(Unknown, State) ->
	error_logger:warning_msg("Received unknown message (~p)~n", [Unknown]),
	{noreply, State}.

code_change(_OldVsn, State, _Extra) -> {ok, State}.

terminate(_Reason, _State) -> ok.
