%%%-------------------------------------------------------------------
%%%
%%% API pattern wraps the actors behind a synchronous api,
%%% simplifying the system usage by not making the users
%%% care about the exact protocol between actors.
%%%
%%% @author drozdyuk
%%% @copyright (C) 2016, <Choven>
%%% @doc
%%%
%%% @end
%%% Created : 15. Jan 2016 12:13 AM
%%%-------------------------------------------------------------------
-module(api_tests).
-author("drozdyuk").
-import(pingpong, [ping/1, start/0]).
-include_lib("eunit/include/eunit.hrl").

pingpong_test() ->
  Pid = start(),
  Start = erlang:timestamp(),
  R1 = ping(Pid),
  R2 = ping(Pid),
  R3 = ping(Pid),
  R4 = ping(Pid),
  End = erlang:timestamp(),
  ?assertEqual(R1, pong),
  ?assertEqual(R2, pong),
  ?assertEqual(R3, pong),
  ?assertEqual(R4, pong),
  Secs = timer:now_diff(End,Start)/1000000,
  io:format("Note that the time it took is cumulative of 4x1seconds = ~p seconds", [Secs]).
