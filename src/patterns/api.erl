%%%-------------------------------------------------------------------
%%% @author myrakumka
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 14. Jan 2016 11:12 PM
%%%-------------------------------------------------------------------
-module(api).
-author("myrakumka").
-import(timer, [sleep/1]).

%% API
-export([ping/1, pingpongloop/0, start/0]).

ping(Pid)->
  Pid ! {self(), ping},
  receive
    {Pid, Msg} -> Msg
  end.

start() -> spawn(?MODULE, pingpongloop, []).

pingpongloop() ->
  receive
    {From, ping} ->
      % Sleep for a second
      sleep(1000),
      From ! {self(), pong},
      pingpongloop()
  end.

